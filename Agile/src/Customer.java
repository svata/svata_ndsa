import java.sql.ResultSet;

public class Customer {
	public GUI g;
	private Database db;
	public void connectToGUI(GUI g){
		this.g=g;
	}
	public Customer(){
		db=new Database();
	}
	public String validate(String firstname,String lastname,String birthyear, 
			String address,String phone, String email,String areacode){
		String answer ="";
	//	Calendar now=Calendar.getInstance();
	//	int year=now.get(Calendar.YEAR);
		if(firstname.equals("")||firstname==""){
			answer+= "The firstname is null. \n";
		}
		if(lastname.equals("")||lastname==""){
			answer +="The lastname is null . \n";
		}
		if(birthyear.equals("")||birthyear==""){
			answer+= "The birthyear is null. \n";
		}
		if(birthyear.length()!=4){
			answer+= "The birthyear is  wrong format. \n";
		}
		if(address.equals("")||address==""){
			answer+= "The address is null. \n";
		}
		 if(areacode.equals("")||areacode==""){
			answer+= "The areacode is wrong. \n";
		}
		if(phone.length()!=10||(!phone.matches("[0-9]+"))){
			if(!phone.equals("")||phone!="")
				answer+="The phone number is wrong. \n";  
		}
		if(email.indexOf(".com")==-1||email.indexOf("@")==-1){
			if(email.equals("")||email=="")
				answer+= "The E-mail is null. \n";
			else
				answer+= "The E-mail is wrong. \n";
		}
		return answer;
	}
	public void Create_Customer(String firstname,String lastname,String birthyear, String address,
			String phone, String email,String areacode){
		String answer=validate(firstname,lastname,birthyear, address,phone,email,areacode);	
		if(answer=="")
			db.addNewCustomer(firstname, lastname,address, email, phone,email,areacode);
		else
			g.createCustomer("false",answer);
	}
	public void Search_Customer(String choice,int choiceflag ){
		switch (choiceflag){
 		case 1: db.searchCustomer(choice);break;
		case 2:db.searchCustomerId(choice);break;
		case 3:db.searchCustomerAddress(choice);break;
		case 4:db.searchCusomerSubscriptionId(choice);break;
		}
	}
	/*
	public void Update_Customer(String customerid,String firstname,String lastname,String birthyear, String address,
			String phone, String email,String areacode){
		String answer=validate(firstname,lastname,birthyear, address,phone,email,areacode);	
		if(answer==""){
			db.updateCustomer(customerid,firstname, lastname,address, email, phone);
			g.createNewCustomer("OK");
			}
		else
			g.createNewCustomer(answer);
	}

	public void Achieve_Customer(String customerid){
		db.deleteCustomer(customerid);
	}*/
}
