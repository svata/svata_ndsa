import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;


import javax.swing.JOptionPane;

public class Database 
{
	/** Database Connection */
    private Connection con;
    
    
    /** Used for executing database statements*/
    private Statement stmt;      
    
    /** Holds result of stmt, moves to next row in database*/
    private ResultSet rs;
    
    /** Holds the number of rows in database value*/
    int count;
    
    /** Holds the current number of the row in the database we are on*/
    int current;
	
	
	private String cus_firstName;
	private String cus_lastName;
	private String cus_address;
	private String cus_email;
	private String cus_phone;
	private String cus_id;
	private String cus_Year;
	private String cus_area;

	
	private String sub;
	private String cus;
	private int [] publications;
	private String hEnd;
	private String sub_Id;
	
	private GUI gui;
	
    public Database()
    {
        con = null;         /** @param con       the connection parameter to connect to MS Access database.*/
        stmt = null;        /** @param stmt      the statement parameter to execute database statements.*/
        rs = null;          /** @param rs        the result parameter to hold the result of statement and move rows.*/
        count = 0;          /** @param count     the int value to count number of rows in database.*/
        current = 0;        /** @param current   the int value to keep track of which row in database we are on.*/
    
        dbConn();// method to connect to database using odbc-jdbc
        initDB();// method to initialise gui with database info

    }
    public void connectToGui(GUI gui) {
    	this.gui = gui;
    }
    
    private void initDB() {
		// TODO Auto-generated method stub
		
	}

	public void dbConn()
    {
		try		
		{
				// driver to use with named database which is stored alongside program in C Drive
		        String url = "jdbc:ucanaccess://c:/BNotes/backEnd.accdb";
		        
		        // connection represents a session with a specific database
		        con = DriverManager.getConnection(url);
		
		        // stmt used for executing sql statements and obtaining results
		        stmt = con.createStatement();
		        System.out.println("Connected");
		        rs = stmt.executeQuery("SELECT * FROM Customer");
		
				while(rs.next())	// count number of rows in table
		        {
		            count++;
		        }
		        System.out.println(count);
		        //String i = count.toString();
		     // gui.createCustomer(, count);
		        
		        rs.close();
		}
		catch(Exception e) 
		{
			System.out.println("Unable to connect to database");
			/*JOptionPane.showMessageDialog(database.this,"Error in startup.","Error", JOptionPane.PLAIN_MESSAGE);} */
		}		
    }


public void addNewCustomer(String firstName, String lastName,String YearOfBirth, String address, String email, String phone, String Area) {
	boolean check=false;
String new_id="";
String new_subid="";
	
	try 
        {
		this.cus_Year = YearOfBirth;
		this.cus_firstName = firstName;
		this.cus_lastName = lastName;
		this.cus_address = address;
		this.cus_email = email;
		this.cus_phone = phone;
		this.cus_area = Area;
		
		
		
		String checkEmail = "SELECT Email FROM Customer";
		rs = stmt.executeQuery(checkEmail);
			while(rs.next())
	        {
				if(cus_email.equals(rs.getString("Email"))) {

					check = true;
					break;
				}
				
	            
	        }
			
		if (check == true) {
			
			new_id = "fail";
			new_subid= "Email already existed";
		}else {
			String customer = "INSERT INTO Customer(FirstName, LastName, YearOfBirth, Address, Email, PhoneNumber, Area)VALUES('"+cus_firstName+"', '"+cus_lastName+"', '"+cus_Year+"', '"+cus_address+"', '"+cus_email+"', '"+cus_phone+"', '"+cus_area+"')";
			rs = stmt.executeQuery(customer);
			//count++;
			
			String cusIdt = "Select c.Cus_Id, s.Sub_Id from Customer c, Subscription s where c.Cus_Id = s.Cus_Id order by Cus_Id desc limit 1";
			rs = stmt.executeQuery(cusIdt);
				while(rs.next())	// count number of rows in table
		        {
		            count++;    
					new_id=rs.getString(1);
					new_subid=rs.getString(2);
		        }
				
			}
		
		//gui.createCustomer(new_id, new_subid);	
	}
	catch (SQLException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
		new_id = "fail";
		new_subid= e.toString();	
	}finally {
		gui.createCustomer(new_id, new_subid);
	}
}


public void updateCustomer(String id, String firstName, String lastName, String YearOfBirth, String address, String email, String phone, String Area)
//boolean cus=false;
{
	String message = "";
try 
    {
	this.cus_area = Area;
	this.cus_id = id;
	this.cus_firstName = firstName;
	this.cus_lastName = lastName;
	this.cus_address = address;
	this.cus_email = email;
	this.cus_phone = phone;
	this.cus_Year = YearOfBirth;
	boolean found = false;
	
	
	String checkId = "SELECT Cus_Id FROM Customer";
	rs = stmt.executeQuery(checkId);
		while(rs.next())
        {
			if(cus_id.equals(rs.getString("Cus_Id"))) {
				found = true;
			}  
        }
		if (found != true) {
		 message = "Id doesn't exist";
		}else {
		
		
		String updateTemp = "UPDATE Customer SET "+"FirstName = '"+cus_firstName+"', LastName = '"+cus_lastName+"', Address = '"+cus_address+"', Email ='"+cus_email+"', PhoneNumber = '"+cus_phone+"' where Cus_Id = "+cus_id;
		
		rs = stmt.executeQuery(updateTemp);
		System.out.println("Customer has been altered");
		
		}	
		message = "Updates were carried on";
	}
    catch (SQLException e) 
    {
		// TODO Auto-generated catch block
		e.printStackTrace();
		message =e.toString();
		
	}
finally{
	//gui.UpdateCustomer(cus_id, message);
}
}


public void deleteCustomer(String id) {
//boolean cus=false;
String message = "";

try 
  {
	this.cus_id = id;
	boolean found = false;
	String checkId = "SELECT Cus_Id FROM Customer";
	rs = stmt.executeQuery(checkId);
		while(rs.next())
        {
			if(cus_id.equals(rs.getString("Cus_Id"))) {
				found = true;
			}  
        }
		if (found != true) {
			message = "customer doesn't exist";
		//gui.DeleteCustomer(cus_id,message);
		}else {

		
		String deleteTemp = "DELETE From Customer where Cus_Id = "+cus_id;
		rs = stmt.executeQuery(deleteTemp);
		count--;
		
		String deletesub = "DELETE From Subscription where Cus_Id = "+cus_id;
		rs = stmt.executeQuery(deletesub);
		count--;
		System.out.println();
		
		}	
		message = "Customer has been deleted";
	}
  catch (SQLException e) 
  {
		// TODO Auto-generated catch block
		e.printStackTrace();
		message = e.toString();
	}finally {
	//gui.DeleteCustomer(cus_id,message);
	}
}

public void addNewSubscription(String subId, String cusId, int[] pub){
	String new_subid="";
	String message = "";
try 
    {
	this.sub = subId;
	this.cus = cusId;
	this.publications= pub;
	
		String subscription = "INSERT INTO Subscription(Sub_Id, Cus_Id, Newspaper1, Newpaper2, Newspaper3, Newpaper4, Newpaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3)VALUES('"+sub+"', '"+cus+"', '"+publications[0]+"', '"+publications[1]+"', '"+publications[2]+"', '"+publications[3]+"', '"+publications[4]+"', '"+publications[5]+"', '"+publications[6]+"', '"+publications[7]+"', '"+publications[8]+"', '"+publications[9]+"', '"+publications[10]+"', '"+publications[11]+"', '"+publications[12]+"')";
		rs = stmt.executeQuery(subscription);
		System.out.println("New subscription has been added");
		
		
		String subIdt = "Select Sub_Id from Subscription order by Sub_Id desc limit 1";
		rs = stmt.executeQuery(subIdt);
			while(rs.next())	// count number of rows in table
	        {
	            count++;    
				new_subid=rs.getString(1);
	        }
			message = "insert was successful";

	}
    catch (SQLException e) 
    {
		// TODO Auto-generated catch block
		e.printStackTrace();
		message = e.toString();
	}finally {
		//gui.addNewSubscription(new_subid,message);
	}
}

public void updateSuscription(String subId, String cusId, int[] pub){
	String message = "";
try 
    {
	this.sub = subId;
	this.cus = cusId;
	this.publications= pub;
	boolean found = false;
	


	String checkId = "SELECT Sub_Id FROM Subscription";
	rs = stmt.executeQuery(checkId);
	while(rs.next())
    {
		if(checkId.equals(rs.getString("Sub_Id"))) {
			found = true;
		}  
    }
	if (found != true) {
		message = "Subscription id doesn't exist";
		//gui.UpdateSubscription(sub_id, message);
	}else {
		String updateTemp = "UPDATE Subscription SET " + "Sub_Id = '"+sub+"', Cus_Id = '"+cus+"', Newspaper1 = '"+publications[0]+"',Newpaper2 = '"+publications[1]+"', Newspaper3 = '"+publications[2]+"', Newpaper4 = '"+publications[3]+"', Newpaper5 = '"+publications[4]+"', Magazine1 = '"+publications[5]+"', Magazine2 = '"+publications[6]+"',Magazine3 = '"+publications[7]+"', Magazine4 = '"+publications[8]+"', Magazine5 = '"+publications[9]+"',MagazineM1 = '"+publications[10]+"', MagazineM2 = '"+publications[11]+"', MagazineM3 = '"+publications[12]+"' where Sub_Id = "+sub_Id;
		
		rs = stmt.executeQuery(updateTemp);
		System.out.println("Customer has been altered");
		
	}
	message = "Customer has been altered";
}
  catch (SQLException e) 
  {
		// TODO Auto-generated catch block
		e.printStackTrace();
		message = e.toString();
		
	}finally {
	//gui.UpdateSubscription(sub_id,message);
	}
}



public void searchCustomer(String nameToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.LastName = '"+nameToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String Age = rs.getString("YearOfBirth");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String Cusarea = rs.getString("Area");
		String CusSubId = rs.getString("Sub_Id");
		
		if(CusName.equals(nameToFind)) {	
			gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea, CusSubId);
			found = true;
		}
	}
	rs.close();
}
catch(Exception e) {
	gui.searchCustomer(null,"Error in finding customer in database", null, null, null, null, null, null, null);
}
}



public void searchCustomerId(String idToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.Cus_Id = '"+idToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String Age = rs.getString("YearOfBirth");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String Cusarea = rs.getString("Area");
		String CusSubId = rs.getString("Sub_Id");
		
		if(CusId.equals(idToFind)) {
			
			gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea, CusSubId);
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {gui.searchCustomer(null,"Error in finding customer in database", null, null, null, null, null, null, null);}
}

public void searchCustomerAddress(String addressToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.Address = '"+addressToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String Age = rs.getString("YearOfBirth");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String Cusarea = rs.getString("Area");
		String CusSubId = rs.getString("Sub_Id");
		
		if(CusId.equals(addressToFind)) {
			
			gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea, CusSubId);
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {gui.searchCustomer(null,"Error in finding customer in database", null, null, null, null, null, null, null);}
}

public void searchCustomerSubId(String SubIdToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and s.Sub_Id = '"+SubIdToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String Age = rs.getString("YearOfBirth");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String Cusarea = rs.getString("Area");
		String CusSubId = rs.getString("Sub_Id");
		
		if(CusId.equals(SubIdToFind)) {
			
			gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea, CusSubId);
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {gui.searchCustomer(null,"Error in finding customer in database", null, null, null, null, null, null, null);}
}

}
